package rmi.client;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.logging.Level;
import java.util.logging.Logger;
import rmi.libs.ThuatToan;

public class Client {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        new Client().run();
    }
  
    private void run(){
        int port= 6394;
        int[] array = new int[15];
        java.util.Random rn = new java.util.Random();
        System.out.println("Dãy số ngẫu nhiên:");
        for (int i=0; i < array.length; i++) array[i] = rn.nextInt(20) + 1;
        for (int i=0; i < array.length; i++) System.out.print( array[i] + " " );
        System.out.println();
        try {
            System.out.println("Kết quả: Dãy tăng dần:");                 
            ThuatToan nguyenTo = (ThuatToan) Naming.lookup("rmi://localhost:"+port+"/ThuatToan");
            array=nguyenTo.SapXep(array);

            for (int i=0; i < array.length; i++) System.out.print( array[i] + " " ); 
            System.out.println();
            
        } catch (NotBoundException | MalformedURLException | RemoteException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        }
           
    }
}
